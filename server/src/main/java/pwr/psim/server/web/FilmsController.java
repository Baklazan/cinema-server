package pwr.psim.server.web;

import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import pwr.psim.server.model.Showing;
import pwr.psim.server.model.User;
import pwr.psim.server.service.ShowingService;
import pwr.psim.server.service.UserService;

@Controller
public class FilmsController {
	@Autowired
	private ShowingService showingService;
	
	@Autowired
	private UserService userService;
	
	@RequestMapping(value = "/films",  method = RequestMethod.GET)
	public ModelAndView welcome(Model model, Principal principal) {
		User user = null;
		if (principal != null) {
			String username = principal.getName();
			user = userService.findByUsername(username);
		}
		
		model.addAttribute("user", user);
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		List<Showing> showingList = showingService.findByShowingDate(Integer.parseInt(sdf.format(new Date())));
		
		return new ModelAndView("films", "currentlyShowing", showingList);
    }
}
