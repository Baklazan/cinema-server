package pwr.psim.server.web;

import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import pwr.psim.server.model.Movie;
import pwr.psim.server.model.User;
import pwr.psim.server.service.MovieService;
import pwr.psim.server.service.UserService;

@Controller
public class IndexController {
	@Autowired
	private UserService userService;
	
	@Autowired
	private MovieService movieService;
	
	@RequestMapping(value = {"/", "/index"}, method = RequestMethod.GET)
    public ModelAndView welcome(Model model, Principal principal) {
		User user = null;
		if (principal != null) {
			String username = principal.getName();
			user = userService.findByUsername(username);
		}
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		List<Movie> currentlyPlaying = movieService.findByDate(Integer.parseInt(sdf.format(new Date())));
		model.addAttribute("user", user);
		
        return new ModelAndView("index", "currentlyPlaying", currentlyPlaying);
    }
	
	
}
