package pwr.psim.server.web;

import java.io.File;
import java.io.IOException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import pwr.psim.server.model.Hall;
import pwr.psim.server.model.Movie;
import pwr.psim.server.model.NarrationType;
import pwr.psim.server.model.NewShowing;
import pwr.psim.server.model.Showing;
import pwr.psim.server.model.TechnologyType;
import pwr.psim.server.model.User;
import pwr.psim.server.service.HallService;
import pwr.psim.server.service.MovieService;
import pwr.psim.server.service.ShowingService;
import pwr.psim.server.service.UserService;

@Controller
@RequestMapping(value = "/adminPanel")
public class AdminPanelController {
	@Autowired
	private MovieService movieService;
	
	@Autowired
	private HallService hallService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private ShowingService showingService;
	
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView showPage(Model model, Principal principal) {
		User user = null;
		if (principal != null) {
			String username = principal.getName();
			user = userService.findByUsername(username);
		}
		
		model.addAttribute("user", user);
		return new ModelAndView("adminPanel");
    }
	
	@RequestMapping(value = "/addNewMovie", method = RequestMethod.GET)
	public ModelAndView showAddNewMovie(Model model, Principal principal) {
		User user = null;
		if (principal != null) {
			String username = principal.getName();
			user = userService.findByUsername(username);
		}
		
		model.addAttribute("user", user);
		
		List<TechnologyType> technologyType = new ArrayList<>(Arrays.asList(TechnologyType.values()));
		
		return new ModelAndView("adminPanelAddNewMovie", "technologyType", technologyType);
	}
	
	@RequestMapping(value = "/addNewShowing", method = RequestMethod.GET)
	public ModelAndView showAddNewShowing(Model model, Principal principal) {
		User user = null;
		if (principal != null) {
			String username = principal.getName();
			user = userService.findByUsername(username);
		}
		
		model.addAttribute("user", user);
		
		List<Movie> movies = movieService.findAll();
		List<Hall> halls = hallService.findAll();
		List<TechnologyType> technologyType = new ArrayList<>(Arrays.asList(TechnologyType.values()));
		List<NarrationType> narrationType = new ArrayList<>(Arrays.asList(NarrationType.values()));
		
		NewShowing showings = new NewShowing(movies, halls, technologyType, narrationType);
		return new ModelAndView("adminPanelAddNewShowing", "showings", showings);
	}
	
	@ResponseBody
	@RequestMapping(value = "/saveNewMovie", method = RequestMethod.POST)
	public Object saveNewMovie(Model model, MultipartHttpServletRequest  request, @RequestParam("poster") MultipartFile posterFile, @RequestParam("screen") MultipartFile screenFile) {
		
		String title = request.getParameter("title");
		String director = request.getParameter("director");
		String starring = request.getParameter("starring");
		Long productionCost = Long.parseLong(request.getParameter("productionCost"));
		TechnologyType technology = TechnologyType.valueOf(request.getParameter("technology"));
		String description = request.getParameter("description");
		Integer showingStart = Integer.parseInt(request.getParameter("showingStart").replace("-", ""));
		System.out.println(showingStart);
		Integer showingEnd = Integer.parseInt(request.getParameter("showingEnd").replace("-", ""));
		String poster = "";
		String screen = "";
		Integer length = Integer.parseInt(request.getParameter("length"));
		 
		if (!posterFile.isEmpty()) {
			String uploadsDir = "/resources/images/posters/";
            String realPathToUploads =  request.getServletContext().getRealPath(uploadsDir) + "\\";
            if(!new File(realPathToUploads).exists())
            {
                new File(realPathToUploads).mkdir();
            }
            
            String fileName = posterFile.getOriginalFilename();
            String posterEndPath = realPathToUploads + fileName;
            File dest = new File(posterEndPath);
            try {
				posterFile.transferTo(dest);
			} catch (IllegalStateException | IOException e) {
				e.printStackTrace();
			}
            
            screen = uploadsDir + fileName;
		}
		
		if (!screenFile.isEmpty()) {
			String uploadsDir = "/resources/images/screens/";
            String realPathToUploads =  request.getServletContext().getRealPath(uploadsDir) + "\\";
            if(!new File(realPathToUploads).exists())
            {
                new File(realPathToUploads).mkdir();
            }
            
            String fileName = screenFile.getOriginalFilename();
            String stringEndPath = realPathToUploads + fileName;
            File dest = new File(stringEndPath);
            try {
            	screenFile.transferTo(dest);
			} catch (IllegalStateException | IOException e) {
				e.printStackTrace();
			}
            
            poster = uploadsDir + fileName;
		}
		
		Movie movie = new Movie(director, starring, productionCost, technology, showingStart, showingEnd, description, title, screen, poster, length);
		movieService.save(movie);
		
		return new ModelAndView("adminPanel");
	}
	
	@ResponseBody
	@RequestMapping(value = "/saveNewShowing", method = RequestMethod.POST)
	public Object saveNewMovie(Model model, HttpServletRequest  request) {
		Long movieId = Long.parseLong(request.getParameter("movieId"));
		Movie movie = movieService.findById(movieId);
		Integer showingDay = Integer.parseInt(request.getParameter("showingDay").replaceAll("-", ""));
		Integer showingTime = Integer.parseInt(request.getParameter("showingHour").replaceAll(":", ""));
		Long hallId = Long.parseLong(request.getParameter("hall"));
		Hall hall = hallService.findById(hallId);
		
		TechnologyType technology = TechnologyType.valueOf(request.getParameter("technology"));
		NarrationType narration = NarrationType.valueOf(request.getParameter("narration"));
		
		Showing showing = new Showing(showingDay, movie, hall, showingTime, narration, technology);
		
		showingService.save(showing);
		
		return null;
	}
}
