package pwr.psim.server.web;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import pwr.psim.server.model.Hall;
import pwr.psim.server.model.Showing;
import pwr.psim.server.model.Ticket;
import pwr.psim.server.model.TicketType;
import pwr.psim.server.model.User;
import pwr.psim.server.service.ShowingService;
import pwr.psim.server.service.TicketService;
import pwr.psim.server.service.UserService;

@Controller
public class SelectSeatController {
	@Autowired
	private ShowingService showingService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private TicketService ticketService;
	
	@RequestMapping(value = "/selectSeat",  method = RequestMethod.GET)
	public ModelAndView welcome(Model model, Principal principal, HttpServletRequest request) {
		User user = null;
		if (principal != null) {
			String username = principal.getName();
			user = userService.findByUsername(username);
		}
		
		model.addAttribute("user", user);
		
		Long showingId = Long.parseLong(request.getParameter("showingId"));
		Showing showing = showingService.findById(showingId);
		Hall hall = showing.getHall();
		
		List<TicketType> ticketType = new ArrayList<>(Arrays.asList(TicketType.values()));
		List<Ticket> chosenSeats = ticketService.findByShowing(showing);
		
		String reservedSeats = "";
		for(Ticket myTicket : chosenSeats) {
			reservedSeats += myTicket.getSeat() + ";";
		}
		
		model.addAttribute("showingId", showingId);
		model.addAttribute("hallId", hall.getId());
		model.addAttribute("seatsCount", hall.getSeatsCount());
		model.addAttribute("ticketType", ticketType);
		model.addAttribute("chosenSeats", reservedSeats);
		
		return new ModelAndView("selectSeat");
    }
	
	@RequestMapping(value = "/selectSeat/saveSeat",  method = RequestMethod.POST)
	public ModelAndView saveSeat(Model model, Principal principal, HttpServletRequest request) {
		Long showingId = Long.parseLong(request.getParameter("showingId"));
		Showing showing = showingService.findById(showingId);
		User user = userService.findByUsername(request.getParameter("username"));
		TicketType ticketType = TicketType.valueOf(request.getParameter("ticketType"));
		String seatId = request.getParameter("seatId");
		String[] seatsChosen = seatId.split(";");
		
		for (String mySeat : seatsChosen) {
			Ticket ticket = new Ticket(showing, 0, ticketType, user, mySeat);
			ticketService.save(ticket);
		}
		
		return null;
    }
}
