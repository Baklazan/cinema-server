package pwr.psim.server.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pwr.psim.server.model.Hall;
import pwr.psim.server.repository.HallRepository;

@Service
public class HallServiceImpl implements HallService {
	@Autowired
	private HallRepository hallRepository;
	
	@Override
	public List<Hall> findAll() {
		return hallRepository.findAll();
	}

	@Override
	public Hall findById(Long hallId) {
		return hallRepository.findById(hallId);
	}
}
