package pwr.psim.server.service;

import java.util.List;

import pwr.psim.server.model.Showing;

public interface ShowingService {
	void save(Showing showing);
	List<Showing> findByShowingDate(Integer showingDate);
	Showing findById(Long showingId);
}
