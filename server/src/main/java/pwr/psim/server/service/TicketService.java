package pwr.psim.server.service;

import java.util.List;

import pwr.psim.server.model.Showing;
import pwr.psim.server.model.Ticket;
import pwr.psim.server.model.User;

public interface TicketService {
	List<Ticket> findByShowing(Showing showing);
	void save(Ticket ticket);
	List<Ticket> findByUser(User user);
}
