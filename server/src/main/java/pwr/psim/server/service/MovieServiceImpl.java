package pwr.psim.server.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pwr.psim.server.model.Movie;
import pwr.psim.server.repository.MovieRepository;

@Service
public class MovieServiceImpl implements MovieService {
	@Autowired
    private MovieRepository movieRepository;

    @Override
    public void save(Movie movie) {
        movieRepository.save(movie);
    }
    
    @Override
    public List<Movie> findAll() {
    	return movieRepository.findAll();
    }
    
    @Override
    public List<Movie> findByDate(Integer findDate) {
    	return movieRepository.findByStartDateAndEndDate(findDate);
    }
    
    @Override
    public Movie findById(Long movieId) {
    	return movieRepository.findById(movieId);
    }
}
