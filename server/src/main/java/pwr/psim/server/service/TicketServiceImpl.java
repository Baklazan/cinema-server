package pwr.psim.server.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pwr.psim.server.model.Showing;
import pwr.psim.server.model.Ticket;
import pwr.psim.server.model.User;
import pwr.psim.server.repository.TicketRepository;

@Service
public class TicketServiceImpl implements TicketService {
	@Autowired
	private TicketRepository ticketRepository;
	
	@Override
	public List<Ticket> findByShowing(Showing showing) {
		return ticketRepository.findByShowing(showing);
	}
	
	@Override
	public void save(Ticket ticket) {
		ticketRepository.save(ticket);
	}
	
	@Override
	public List<Ticket> findByUser(User user) {
		return ticketRepository.findByUser(user);
	}

}
