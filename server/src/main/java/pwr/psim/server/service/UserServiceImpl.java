package pwr.psim.server.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import pwr.psim.server.model.Role;
import pwr.psim.server.model.User;
import pwr.psim.server.repository.RoleRepository;
import pwr.psim.server.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService {
	@Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public void createAdmin() {
    	User admin = new User();
    	
    	admin.setUsername("Admin");
    	admin.setPassword(bCryptPasswordEncoder.encode("admin_pass_55123"));
    	admin.setRoles(new HashSet<>(roleRepository.findAll()));
        userRepository.save(admin);
    }
    
    @Override
    public void save(User user) {
    	List<Role> roleList = new ArrayList<>();
    	roleList.add(roleRepository.findOne(1l));
    	
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user.setRoles(new HashSet<>(roleList));
        userRepository.save(user);
    }

    @Override
    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }
}
