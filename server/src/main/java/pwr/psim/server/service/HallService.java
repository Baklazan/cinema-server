package pwr.psim.server.service;

import java.util.List;

import pwr.psim.server.model.Hall;

public interface HallService {
	List<Hall> findAll();
	
	Hall findById(Long hallId);
}
