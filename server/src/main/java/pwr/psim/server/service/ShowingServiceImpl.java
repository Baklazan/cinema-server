package pwr.psim.server.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pwr.psim.server.model.Showing;
import pwr.psim.server.repository.ShowingRepository;

@Service
public class ShowingServiceImpl implements ShowingService {
	@Autowired
	private ShowingRepository showingRepository;
	
	@Override
	public void save(Showing showing) {
		showingRepository.save(showing);
	}
	
	@Override
	public List<Showing> findByShowingDate(Integer showingDate) {
		return showingRepository.findByShowingDate(showingDate);
	}
	
	@Override
	public Showing findById(Long showingId) {
		return showingRepository.findById(showingId);
	}
}
