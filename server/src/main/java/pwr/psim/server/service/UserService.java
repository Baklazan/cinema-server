package pwr.psim.server.service;

import pwr.psim.server.model.User;

public interface UserService {
	void createAdmin();
	
	void save(User user);

    User findByUsername(String username);
}
