package pwr.psim.server.service;

import java.util.List;

import pwr.psim.server.model.Movie;

public interface MovieService {
	void save(Movie movie);
	
	List<Movie> findAll();
	
	List<Movie> findByDate(Integer findDate);
	
	Movie findById(Long movieId);

}
