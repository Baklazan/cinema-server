package pwr.psim.server.singleton;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;

import pwr.psim.server.model.User;
import pwr.psim.server.service.UserService;

public class MySingleton {

	@Autowired
    private UserService userService;
	
    private static volatile MySingleton INSTANCE = null;
    
    private MySingleton(){
		
    }

    
	public static MySingleton getInstance(){
	    if(INSTANCE == null){
	        synchronized(MySingleton.class){
	            if(INSTANCE == null){
	                try{
	                    doWork();
	                }catch(Exception e){
	                    throw new IllegalStateException("xyz", e);
	                }
	                INSTANCE = new MySingleton();
	            }
	        }
	    }
	
	    return INSTANCE;
	}
	
	private static void doWork() {
	    System.out.println("Cos robi");
    }
	
	@PostConstruct
	private void createAdmin() {
		User user = userService.findByUsername("Admin");
	    
	    if (user == null) {
	    	userService.createAdmin();
	    }
	}

}