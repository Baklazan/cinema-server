package pwr.psim.server.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import pwr.psim.server.model.Role;

public interface RoleRepository extends JpaRepository<Role, Long>{
	
}
