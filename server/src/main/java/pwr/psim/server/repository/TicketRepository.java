package pwr.psim.server.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import pwr.psim.server.model.Showing;
import pwr.psim.server.model.Ticket;
import pwr.psim.server.model.User;

@Repository
public interface TicketRepository extends JpaRepository<Ticket, Long>{
	List<Ticket> findByShowing(Showing showing);
	List<Ticket> findByUser(User user);
}
