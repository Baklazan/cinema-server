package pwr.psim.server.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import pwr.psim.server.model.User;

public interface UserRepository extends JpaRepository<User, Long>{
	User findByUsername(String username);
}
