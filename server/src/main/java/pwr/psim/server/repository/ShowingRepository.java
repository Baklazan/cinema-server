package pwr.psim.server.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import pwr.psim.server.model.Showing;

@Repository
public interface ShowingRepository extends JpaRepository<Showing, Long> {
	List<Showing> findByShowingDate(Integer showingDate);
	Showing findById(Long id);
}
