package pwr.psim.server.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import pwr.psim.server.model.Movie;

public interface MovieRepository extends JpaRepository<Movie, Long>{
	
	@Query("SELECT m FROM Movie m WHERE m.showingStart <= ?1 AND m.showingEnd >= ?1")
	List<Movie> findByStartDateAndEndDate(Integer findDate);
	
	Movie findById(Long id);
}
