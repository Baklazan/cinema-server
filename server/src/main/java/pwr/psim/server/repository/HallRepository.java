package pwr.psim.server.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import pwr.psim.server.model.Hall;

@Repository
public interface HallRepository extends JpaRepository<Hall, Long> {
	Hall findById(Long id);
}
