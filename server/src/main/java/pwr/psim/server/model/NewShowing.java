package pwr.psim.server.model;

import java.util.List;

public class NewShowing {
	
	private List<Movie> movieList;
	private List<Hall> hallList;
	private List<TechnologyType> technologyType;
	private List<NarrationType> narrationType;
	
	public NewShowing(List<Movie> movieList, List<Hall> hallList, List<TechnologyType> technologyType, List<NarrationType> narrationType) {
		this.movieList = movieList;
		this.hallList = hallList;
		this.technologyType = technologyType;
		this.narrationType = narrationType;
	}

	public List<Movie> getMovieList() {
		return movieList;
	}

	public void setMovieList(List<Movie> movieList) {
		this.movieList = movieList;
	}

	public List<Hall> getHallList() {
		return hallList;
	}

	public void setHallList(List<Hall> hallList) {
		this.hallList = hallList;
	}

	public List<TechnologyType> getTechnologyType() {
		return technologyType;
	}

	public void setTechnologyType(List<TechnologyType> technologyType) {
		this.technologyType = technologyType;
	}

	public List<NarrationType> getNarrationType() {
		return narrationType;
	}

	public void setNarrationType(List<NarrationType> narrationType) {
		this.narrationType = narrationType;
	}
}
