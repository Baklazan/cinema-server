package pwr.psim.server.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

@Entity
@Table(name = "movie")
public class Movie {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String director;
	@Lob
	private String starring;
	private Long productionCost;
	private TechnologyType technology;
	private Integer showingStart;
	private Integer showingEnd;
	private String poster;
	@Lob
	private String description;
	private String title;
	private String screen;
	private int length;
	private transient String showingStartString;
	private transient String showingEndString;
	
	public Movie(String director, String starring, Long productionCost, TechnologyType technology, Integer showingStart, Integer showingEnd, String description, String title, String screen, String poster, int length) {
		this.director = director;
		this.starring = starring;
		this.productionCost = productionCost;
		this.technology = technology;
		this.showingStart = showingStart;
		this.showingEnd = showingEnd;
		this.description = description;
		this.title = title;
		this.screen = screen;
		this.poster = poster;
		this.length = length;
		this.showingStartString = showingStart.toString().substring(3) + "-" + showingStart.toString().substring(4, 5) + "-" + showingStart.toString().substring(6, 7);
		this.showingEndString = showingEnd.toString().substring(3) + "-" + showingEnd.toString().substring(4, 5) + "-" + showingEnd.toString().substring(6, 7);
	}
	
	public Movie(){}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	public String getStarring() {
		return starring;
	}

	public void setStarring(String starring) {
		this.starring = starring;
	}

	public Long getProductionCost() {
		return productionCost;
	}

	public void setProductionCost(Long productionCost) {
		this.productionCost = productionCost;
	}

	public TechnologyType getTechnology() {
		return technology;
	}

	public void setTechnology(TechnologyType technology) {
		this.technology = technology;
	}

	public Integer getShowingStart() {
		return showingStart;
	}

	public void setShowingStart(Integer showingStart) {
		this.showingStart = showingStart;
	}

	public Integer getShowingEnd() {
		return showingEnd;
	}

	public void setShowingEnd(Integer showingEnd) {
		this.showingEnd = showingEnd;
	}

	public String getPoster() {
		return poster;
	}

	public void setPoster(String poster) {
		this.poster = poster;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getScreen() {
		return screen;
	}

	public void setScreen(String screen) {
		this.screen = screen;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public String getShowingStartString() {
		return String.valueOf(showingStart).substring(0,4) + "-" + String.valueOf(showingStart).substring(4, 6) + "-" + String.valueOf(showingStart).substring(6, 8);
	}

	public void setShowingStartString(String showingStartString) {
		this.showingStartString = showingStartString;
	}

	public String getShowingEndString() {
		return String.valueOf(showingEnd).substring(0,4) + "-" + String.valueOf(showingEnd).substring(4, 6) + "-" + String.valueOf(showingEnd).substring(6, 8);
	}

	public void setShowingEndString(String showingEndString) {
		this.showingEndString = showingEndString;
	}
}
