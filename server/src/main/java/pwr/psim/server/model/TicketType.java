package pwr.psim.server.model;

public enum TicketType {
	
	NORMAL,
	DISCOUNT,
	GROUP,
	;
	
	private TicketType(){
		
	}
}
