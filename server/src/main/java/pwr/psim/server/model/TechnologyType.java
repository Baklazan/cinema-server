package pwr.psim.server.model;

public enum TechnologyType {
	D2("2D"),
	D3("3D"),
	BOTH("2D/3D"),
	;
	
	private String type;
	
	private TechnologyType(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}
