package pwr.psim.server.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "showing")
public class Showing {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private Integer showingDate;
	private NarrationType narrationType;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "movie_id")
	private Movie movie;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "hall_id")
	private Hall hall;
	private Integer showingTime;
	private TechnologyType technologyType;
	private transient String showingDateString;
	private transient String showingTimeString;
	
	public Showing(Integer showingDate, Movie movie, Hall hall, Integer showingTime, NarrationType narrationType, TechnologyType technologyType) {
		this.showingDate = showingDate;
		this.movie = movie;
		this.hall = hall;
		this.showingTime = showingTime;
		this.narrationType = narrationType;
		this.technologyType = technologyType;
	}
	
	public Showing(){}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getShowingDate() {
		return showingDate;
	}

	public void setShowingDate(Integer showingDate) {
		this.showingDate = showingDate;
	}

	public NarrationType getNarrationType() {
		return narrationType;
	}

	public void setNarrationType(NarrationType narrationType) {
		this.narrationType = narrationType;
	}

	public Movie getMovie() {
		return movie;
	}

	public void setMovie(Movie movie) {
		this.movie = movie;
	}

	public Hall getHall() {
		return hall;
	}

	public void setHall(Hall hall) {
		this.hall = hall;
	}

	public Integer getShowingTime() {
		return showingTime;
	}

	public void setShowingTime(Integer showingTime) {
		this.showingTime = showingTime;
	}

	public TechnologyType getTechnologyType() {
		return technologyType;
	}

	public void setTechnologyType(TechnologyType technologyType) {
		this.technologyType = technologyType;
	}

	public String getShowingDateString() {
		return String.valueOf(showingDate).substring(0,4) + "-" + String.valueOf(showingDate).substring(4, 6) + "-" + String.valueOf(showingDate).substring(6, 8);
	}

	public void setShowingDateString(String showingDateString) {
		this.showingDateString = showingDateString;
	}

	public String getShowingTimeString() {
		return String.valueOf(showingTime).substring(0,2) + ":" + String.valueOf(showingTime).substring(2,4);
	}

	public void setShowingTimeString(String showingTimeString) {
		this.showingTimeString = showingTimeString;
	}
}
