package pwr.psim.server.thread;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class NewstellerSender {
	@Scheduled(fixedDelay=50000)
	public void doSomething() {
		System.out.println("Sending newsteller");
	}

}
