<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Create an account</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<!-- navbar: include on every site -->
	<nav class="navbar navbar-toggleable-md navbar-light bg-faded">
	  <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
	    <span class="navbar-toggler-icon"></span>
	  </button>
	  <a class="navbar-brand" href="#"><img class="navbar-logo" src="/resources/images/icona.png" /></a>
	  <div class="collapse navbar-collapse" id="navbarNavDropdown">
	    <ul class="navbar-nav">
	      <li class="nav-item">
	        <a class="nav-link" href="/index">Strona główna <span class="sr-only">(current)</span></a>
	      </li>
	      <li class="nav-item">
	        <a class="nav-link" href="/films">Repertuar</a>
	      </li>
	      <li class="nav-item dropdown">
	      
	      <% request.setAttribute("isAdmin", request.isUserInRole("ROLE_ADMIN")); %>
		  <c:if test="${requestScope.isAdmin}">
		  	<!-- Zalogowany admin -->
		    <li class="nav-item">
		        <a class="nav-link" href="/adminPanel">Admin panel</a>
		      </li>
		  </c:if>
		  <c:if test="${!requestScope.isAdmin}">
		      <c:if test="${user.username != null}">
		      <!-- Zalogowany, nie admin -->
		        <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		          ${user.username}
		        </a>
		        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
		          <a class="dropdown-item" href="/myTickets">Moje bilety</a>
		          <!-- <a class="dropdown-item" href="#">Zapisane filmy</a> -->
		          <a class="dropdown-item" href="#">Ustawienia konta</a>
		        </div>
			  </c:if>
			  <c:if test="${user.username == null}">
			  	<!-- Niezalogowany -->
				<a class="nav-link" href="/login">Zaloguj się</a>
			  </c:if>
		  </c:if>
	      
	      </li>
	    </ul>
	    
	    <ul class="navbar-nav navbar-toggler-right">
	    	<li class="nav-item">
	    	<c:if test="${user.username == null}">
	    		<a class="nav-link" href="/register">Zarejestruj sie</a>
    		</c:if>
    		<c:if test="${user.username != null}">
    			<a class="nav-link" href="/login?logout">Wyloguj się</a>
    		</c:if>
	    	</li>
	    </ul>
	  </div>
	</nav>
	<!-- end of navbar -->
<div class="container">

    <form:form method="POST" modelAttribute="userForm" class="form-signin">
        <h2 class="form-signin-heading">Zarejestruj się</h2>
        <spring:bind path="username">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <form:input type="text" path="username" class="form-control" placeholder="Login"
                            autofocus="true"></form:input>
                <form:errors path="username"></form:errors>
            </div>
        </spring:bind>

        <spring:bind path="password">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <form:input type="password" path="password" class="form-control" placeholder="Hasło"></form:input>
                <form:errors path="password"></form:errors>
            </div>
        </spring:bind>

        <spring:bind path="passwordConfirm">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <form:input type="password" path="passwordConfirm" class="form-control"
                            placeholder="Powtórz hasło"></form:input>
                <form:errors path="passwordConfirm"></form:errors>
            </div>
        </spring:bind>

        <button class="btn btn-lg btn-primary btn-block" type="submit">Submit</button>
    </form:form>

</div>
<!-- /container -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
</body>
</html>