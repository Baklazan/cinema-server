<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Wybierz miejsce</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<style>
.chosenSeat {
	background-color:#FFB338;
}
.freeSeat {
	background-color: white;
}
.reservedSeat {
	background-color: #333;
}
</style>
<body>
	<input type="hidden" id="showingId" value="${showingId}">
	<input type="hidden" id="username" value ="${user.username}">
	<div style="background-color:rgb(255, 250, 240); width:100%; height:100%; margin:10px; border-radius:15px">
		<div style="margin-bottom:50px;text-align:center; color: white; border-radius:10px; width:90%; margin:5px 20px; background-color:#333">Ekran</div> 
	  	<div style="width:100%;text-align:center; position:relative;" id="seats"></div>
	  	<br>
	  	<select id="ticketType">
	  		<c:forEach items="${ticketType}" var="type">
	  			<option value="${type}">${type}</option>
	  		</c:forEach>
	  	</select>
	  	<input type="button" value="Kup" onclick="buyTicket()">
	</div>
<!-- required scripts -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
<script>
	$(document).ready(function() {
		var seatsCount = ${seatsCount};
		var seatsVer = seatsCount/10;
		var seatsHor = seatsCount/seatsVer;
		var seatsBreak = seatsHor/2;
		
		for (j=0;j<seatsVer;j++) {
		  for(i=0; i<seatsHor; i++) {
		    if (i == seatsBreak) {
		    	$("#seats").append("<div class=\"freeSeat\" id=\"" + j + "_" + i + "\" onclick=\"reserveSeat(this.id)\" style=\"display:inline-block; margin:2px 1px 2px 50px; width:30px; height:30px; border:2px solid #73AD21; border-radius:5px\"></div>");	
		    } else {
		    	$("#seats").append("<div class=\"freeSeat\" id=\"" + j + "_" + i + "\" onclick=\"reserveSeat(this.id)\" style=\"display:inline-block; position:relative; margin:2px 2px;  width:30px; height:30px; border:2px solid #73AD21;border-radius:5px\"></div>");	
		    }    
		  }
			$("#seats").append("<br>");
		}
		
		var chosenSeats = "${chosenSeats}";
		var reservedSeats = chosenSeats.split(";");
		console.log(reservedSeats);
		
		for (k=0; k<reservedSeats.length - 1; k++) {
			$("#" + reservedSeats[k]).removeClass().addClass("reservedSeat");
			$("#" + reservedSeats[k]).prop("disabled", "disabled");
		}
		
	})
	
	function reserveSeat(id) {
		var currentClass = $("#" + id).attr("class");
		if (currentClass == "freeSeat") {
			$("#" + id).removeClass().addClass("chosenSeat");
		}
		if (currentClass == "chosenSeat") {
			$("#" + id).removeClass().addClass("freeSeat");
		}
		
	}
	
	function buyTicket() {
		var chosenSeats = "";
		$( ".chosenSeat" ).each(function(index) {
			  chosenSeats += $(this).attr("id") + ";";
		});
		
		var ticketType = $("#ticketType").val();
		var showingId = $("#showingId").val();
		var username = $("#username").val();
		
		$.ajax({
			  method: "POST",
			  url: "/selectSeat/saveSeat",
			  data: { ticketType: ticketType, seatId: chosenSeats, showingId: showingId, username: username }
			}).done(function() {
				window.close();
			  });
	}
	
</script>
</body>
</html>