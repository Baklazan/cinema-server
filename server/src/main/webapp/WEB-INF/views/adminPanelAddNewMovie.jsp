<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
	<!-- default header name is X-CSRF-TOKEN -->

    <title>Admin panelt</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
	<!-- navbar: include on every site -->
	<nav class="navbar navbar-toggleable-md navbar-light bg-faded">
	  <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
	    <span class="navbar-toggler-icon"></span>
	  </button>
	  <a class="navbar-brand" href="#"><img class="navbar-logo" src="/resources/images/icona.png" /></a>
	  <div class="collapse navbar-collapse" id="navbarNavDropdown">
	    <ul class="navbar-nav">
	      <li class="nav-item">
	        <a class="nav-link" href="/index">Strona główna <span class="sr-only">(current)</span></a>
	      </li>
	      <li class="nav-item">
	        <a class="nav-link" href="/films">Repertuar</a>
	      </li>
	      <li class="nav-item dropdown">
	      
	      <% request.setAttribute("isAdmin", request.isUserInRole("ROLE_ADMIN")); %>
		  <c:if test="${requestScope.isAdmin}">
		  	<!-- Zalogowany admin -->
		    <li class="nav-item active">
		        <a class="nav-link" href="/adminPanel">Admin panel</a>
		      </li>
		  </c:if>
		  <c:if test="${!requestScope.isAdmin}">
		      <c:if test="${user.username != null}">
		      <!-- Zalogowany, nie admin -->
		        <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		          ${user.username}
		        </a>
		        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
		          <a class="dropdown-item" href="/myTickets">Moje bilety</a>
		          <!-- <a class="dropdown-item" href="#">Zapisane filmy</a> -->
		          <a class="dropdown-item" href="#">Ustawienia konta</a>
		        </div>
			  </c:if>
			  <c:if test="${user.username == null}">
			  	<!-- Niezalogowany -->
				<a class="nav-link" href="/login">Zaloguj się</a>
			  </c:if>
		  </c:if>
	      
	      </li>
	    </ul>
	    
	    <ul class="navbar-nav navbar-toggler-right">
	    	<li class="nav-item">
	    	<c:if test="${user.username == null}">
	    		<a class="nav-link" href="/register">Zarejestruj sie</a>
    		</c:if>
    		<c:if test="${user.username != null}">
    			<a class="nav-link" href="/login?logout">Wyloguj się</a>
    		</c:if>
	    	</li>
	    </ul>
	  </div>
	</nav>
	<!-- end of navbar -->
	<div class="container">
		<form action="/adminPanel/saveNewMovie" method="post" enctype="multipart/form-data" class="form-insert">
			<div class="form-group">
				<label for="title">Tytuł:</label>
				<input type="text" name="title" class="form-control">
			</div>
			<div class="form-group">
				<label for="description">Opis:</label>
				<textarea name="description" rows="4" cols="50" class="form-control"></textarea>
			</div>
			<div class="form-group">
				<label for="director">Reżyser:</label>
				<input type="text" name="director" class="form-control">
			</div>
			<div class="form-group">
				<label for="starring">Obsada:</label>
				<input type="text" name="starring" class="form-control">
			</div>
			<div class="form-group">
				<label for="productionCost">Koszt produkcji:</label>
				<input type="text" name="productionCost" class="form-control">
			</div>
			<div class="form-group">
				<label for="length">Długość:</label>
				<input type="text" name="length" class="form-control">
			</div>
			<div class="form-group">
				<label for="poster">Plakat:</label>
				<input type="file" name="poster" accept="image/*" class="form-control">
			</div>
			<div class="form-group">
				<label for="screen">Screen:</label>
				<input type="file" name="screen" accept="image/*" class="form-control">
			</div>
			<div class="form-group">
				<select name="technology" class="form-control">
				<c:forEach items="${technologyType}" var="type">
					<option value="${type}">${type.type}</option>
				</c:forEach>
				</select>
			</div>
			<div class="form-group">
				<label for="showingStart">Początek pokazów:</label>
				<input type="date" name="showingStart" class="form-control">
			</div>
			<div class="form-group">
				<label for="showingEnd">Koniec pokazów:</label>
				<input type="date" name="showingEnd" class="form-control">
			</div>
			<input  type="submit" class="btn btn-primary">
		</form>
	</div>
	
<!-- required scripts -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
</body>
</html>